"""
This file contains the ModelLoader class definition, which allows to train/test models using using the parameters set
in a .ini configuration file (examples in the cfg folder).
TODO Make the methods get_input_and_target, get_loss, and others related to plots independent to ModelLoader.

"""

import timeit

import cv2 as cv
import numpy as np
import torch.optim as optim

import common as cmf
from dataset_classes import *
from models import *


class ModelLoader:
    def __init__(self, cfg_file=None, train_val=None, test=None):
        """ Constructor of ModelLoader class. Read the cfg file and initialize the required variables training or testing
        a model.
        If the trainVal flag is True, it starts training the model.
        If the test flag is True, it makes the predictions for the test dataset from the trained model (wich could be
        trained right before or in previous moment).

        :param cfg_file: path of the cfg file with the parameters.
        :param train_val: Flag for training the model (assuming that all parameters are set in the cfg file).
        :param test: Test a trained model.
        """
        if cfg_file is None:  # Enter all the parameters manually
            print("No configuration file provided.")

        # self.parms will contain the parameters set up in the .ini file
        self.params = {  # Initialize required params by defalut
            'name': None,  # Trained model name
            'model_type': None,  # unet/acnn
            'device': None,  # gpu/cpu
            'n_epochs': None,  # number of epochs
            'batch_size': None,
            'model_class': None,  # Model class name (located in models.py)
            'acnn_path': None,  # ACNN traned model path for regularization (if needed).
            'acnn_lambda': None,  # Weight used for the ACNN loss.
            'dice_lambda': None,  # Weight used for the Dice loss.
            'ce_lambda': None,  # Weight used for the Cross Entropy loss.
            'train_files_csv': None,  # Datasets csv
            'validation_files_csv': None,
            'test_files_csv': None,
            'models_folder': None,  # Location of the trained models
            'plots_folder': None,  # Location of the plots
            'plot_update_interval': None,  # Frequency of plots update (in epochs)
            'save_dice_plots': None,  # Save Dice coefficient plot over the epochs.
            'autosave_epochs': None,  # Frequency of model checkpoint saving.
            'resume_training': None,  # Continue training the model located in this path
            'optimizer': None,  # Optimizer used for training
            'learning_rate': None,
            'momentum': None,
            'weight_decay': None
        }

        self.data = {
            'train_dataloader': None,
            'validation_dataset': None,
            'validation_dataloader': None,
            'test_dataset': None,
            'test_dataloader': None,
        }

        self.models = {
            'main': None,  # Trained model
            'acnn_model': None  # ACNN model used for regularization (if neccesary)
        }

        self.out_model_path = None  # Output file

        self.train_flag = train_val  # True if we're going to train the model
        self.test_flag = test  # True if we're predicting on test data after (or without) training

        self.cfg_path = cfg_file
        self.params = cmf.set_cfg_params(self.cfg_path, self.params)

        self.train_loss, self.validation_loss, self.test_loss = [], [], []  # Initialize the loss arrays
        self.dice_train, self.dice_validation = [], []
        self.dice_loss_array, self.mse_loss_array, self.bce_loss_array = [], [], []
        self.epoch_loss_array, self.dice_coef_array = [], []

        self.out_model_path = os.path.join(self.params['models_folder'], '{}.pt'.format(self.params['name']))

        self.params['device'] = torch.device("cuda" if torch.cuda.is_available() else "cpu") if str(
            self.params['device']) == 'cuda' else torch.device("cpu")

        self.load_datasets()  # Load the datasets provided in the csv files

        if self.params['train_flag'] is True:
            self.train()

        if self.params['test_flag'] is True:
            self.test()

    def load_datasets(self):
        """
        Load the data, using the Dataset declared in the .ini file (and defined in dataset_classes.py) and initialize
        the DataLoaders, with the batch parameters.
        """
        pin_mem = False if str(self.params['device']) == 'cpu' else True  # Copy Tensors into CUDA pinned memory first.
        dataset_c = eval(self.params['dataset_class'])  # Custom Dataset, defined in dataset_classes and chosen in .ini

        self.data['train_dataloader'] = cmf.get_dataloader(dataset_c, self.params['train_files_csv'],
                                                           self.params['batch_size'],
                                                           required=self.params['train_flag'])
        self.data['validation_dataloader'] = cmf.get_dataloader(dataset_c, self.params['validation_files_csv'],
                                                                self.params['batch_size'])
        self.data['test_dataloader'] = cmf.get_dataloader(dataset_c, self.params['test_files_csv'], batch_size=1,
                                                          required=self.params['test_flag'])

    def train(self):
        """
        Train the model using the previously set up parameters.
        """
        if self.params['resume_training'] != "":  # Continue training a model
            self.models['main'] = torch.load(self.params['resume_training'], map_location=str(self.params['device']))
        else:
            self.models['main'] = eval(self.params['model_class'])().to(self.params['device'])  # Instance a model

        if self.params['acnn_lambda'] != 0:  # If ACNN regularization is needed, load the trained model
            self.models['acnn'] = eval(self.acnn_class)() if self.params['acnn_lambda'] else None
            if self.params['acnn_path']:
                mod = torch.load(self.params['acnn_path'])  # TODO Load the models the same way
                self.models['acnn'].load_state_dict(mod.state_dict())
                self.models['acnn'].eval().to(self.params['device'])  # Set eval mode on (lock the weights)

        # Configure the optimizer
        if self.params['optimizer'] == "adam":
            self.params['optimizer'] = optim.Adam(self.models['main'].parameters(), lr=self.params['learning_rate'],
                                                  weight_decay=self.params['weight_decay'], amsgrad=True)
        elif self.params['optimizer'] == "rmsprop":
            self.params['optimizer'] = optim.RMSprop(self.models['main'].parameters(), lr=self.params['learning_rate'],
                                                     weight_decay=self.params['weight_decay'],
                                                     momentum=self.params['momentum'])
        elif self.params['optimizer'] == "sgd":
            self.params['optimizer'] = optim.SGD(self.models['main'].parameters(), lr=self.params['learning_rate'],
                                                 momentum=self.momentum, weight_decay=self.params['weight_decay'])

        cmf.print_params_dict(self.params)  # Print the params dictionary

        for n_epoch in range(1, self.params['n_epochs'] + 1):  # Training n_epochs
            ep_time = cmf.tic()  # Begin measuring time

            print("Epoch: ", n_epoch)
            self.forward_pass('train', self.data['train_dataloader'])
            self.forward_pass('val', self.data['validation_dataloader'])

            # Calculate remaining time (for display)
            cmf.toc_eps(ep_time, n_epoch, self.params['n_epochs'])  # Stop measuring time and estimate remaining time.

            # Autosave model, configuration and plots
            self.update_plots() if n_epoch % self.params['plot_update_interval'] == 0 else 0  # Update plots if necces.

            if n_epoch % self.params['autosave_epochs'] == 0:
                cmf.save_model(self.models['main'], self.out_model_path, self.cfg_path, n_epoch,
                               save_checkpoint=True)  # Save epoch checkpoint
                self.test()  # Predict on test data
                self.update_plots(epoch_number=n_epoch)  # Update main plots

        cmf.save_model(self.models['main'], self.out_model_path)

    def test(self):
        # If the model isn't loaded yet, load it.
        if self.models['main'] is None:
            if str(self.params['device']) == 'cuda':
                self.models['main'] = torch.load(self.out_model_path)
            else:
                self.models['main'] = torch.load(self.out_model_path, map_location='cpu')

        if self.params['test_files_csv'] == '':
            print("No csv provided for testing")
        else:
            print("Images to test: ", os.path.split(self.params['test_files_csv'])[0])
            cmf.print_params_dict(self.params)  # Print the params dictionary
            self.forward_pass('test', self.data['test_dataloader'])

    def update_plots(self, epoch_number=None):
        """ Update the loss and dice (optional) plots.

        :param epoch_number: If provided, it additionaly generates another image that won't be overwrited after with the
        curves up to that epoch
        """
        if epoch_number is not None:
            out_name = self.params['name'] + '_ep' + str(epoch_number)
        else:
            out_name = self.params['name']

        cmf.array_to_plot([self.train_loss, self.validation_loss],
                          ['train loss', 'validation loss'],
                          name=out_name,
                          plotTitle=self.params['name'] + " model train and val losses",
                          x_label="Epoch", y_label="Loss")

        if self.params['save_dice_plots'] is True:
            cmf.array_to_plot([self.dice_train, self.dice_validation],
                              ['dice train', 'dice validation'],
                              name=out_name,
                              plotTitle=self.params['name'] + " model train and dice coefficients",
                              x_label="Epoch", y_label="Dice")

    def forward_pass(self, phase, data_loader):
        """
        Make a forward pass on the network.

        :param phase: 'train', 'val', 'test'
        :param data_loader: The train/validation/test loader
        :return:
        """
        print("Phase: {}.".format(phase))

        # Clear the epoch losses
        self.epoch_loss_array, self.bce_loss_array, self.mse_loss_array = [], [], []
        self.dice_loss_array, self.dice_coef_array = [], []

        if phase == 'train':
            self.models['main'].train()
        else:  # eval mode (won't update parameters and disable dropout)
            self.models['main'].eval()

        out_folder = ''  # Will be assigned when saving an image

        for batch_idx, sample in enumerate(data_loader):  # for each batch in the split
            # Get the corresponding input and target, which may vary depending on the model
            input_img, target, filepath = self.get_input_and_target(sample, phase)

            if type(input_img) is tuple:
                model_out = []
                for img in input_img:
                    if phase == 'train':
                        img.requires_grad_()
                    model_out.append(self.models['main'](img))
                model_out = tuple(model_out)
            else:
                if phase == 'train':
                    input_img.requires_grad_()
                model_out = self.models['main'](input_img)

            loss = self.get_loss(phase, model_out, target, batch_idx, len(data_loader))

            if phase == 'train':
                loss.backward()
                self.params['optimizer'].step()
                self.params['optimizer'].zero_grad()
            elif phase == 'test':
                out_folder = self.write_predictions(input_img, filepath, model_out, target)

        if phase == 'test':
            print("Output folder: ", out_folder)

        self.loss_avgs(phase)

    def write_predictions(self, input, filepaths, predictions, target=None):
        """ Save batch images to file

        :param input: batch of input images
        :param filepaths: paths of the batch
        :param target: GT of the batch
        :param predictions: prediction of the batch
        :return:
        """
        if type(input) is tuple:
            input = input[0]
        if type(target) is tuple:
            target = target[0]
        if type(predictions) is tuple:
            predictions = predictions[0]

        qty = input.shape[0]
        for i in range(qty):
            path, fname = os.path.split(filepaths[i])
            name, ext = os.path.splitext(fname)
            print("  ", fname)

            input = cmf.hard_segm_from_tensor(input[i].cpu().detach()).numpy().astype(np.uint8)
            output = cmf.hard_segm_from_tensor(predictions[i].cpu().detach()).numpy().astype(np.uint8)
            groundtruth = cmf.hard_segm_from_tensor(target[i].cpu().detach()).numpy().astype(
                np.uint8) if target.sum() else None

            out_folder = cmf.veri_folder(os.path.join(path, "pred_" + self.params['name']))

            cv.imwrite(os.path.join(out_folder, name + ext), input)
            cv.imwrite(os.path.join(out_folder, name + '_p' + ext), output)

            if groundtruth.any():
                cv.imwrite(os.path.join(out_folder, name + '_gt' + ext), groundtruth)

        return out_folder

    def get_input_and_target(self, sample, phase):
        """" Depending the model we're training, the input and target images may change.

        :param sample: BrainSegmentationDataset sample
        :param phase: 'train' 'validation' or 'test'.
        :return:
        """
        filepath = sample['filepath']
        input_img = sample['image'].to(self.params['device'])  # In the general case the input is the image.
        if len(input_img.shape) == 3:  # Binary image
            input_img = input_img.unsqueeze(1)

        # U-NET for segementation: input is an image, GT the segmentation
        if self.params['model_type'] == 'unet':
            ground_truth = sample['mask']
            # target = ground_truth.to(self.params['device'])
            target = cmf.one_hot_encoding(ground_truth).to(self.params['device'])

        # ACNN AE: Input is the noisy segmentation and the output is the same segmentation
        elif self.params['model_type'] == 'acnn':
            noise_fn = [cmf.apply_random_mask, cmf.draw_lines][np.random.randint(0, 2)]
            # noise_fn = cmf.apply_random_mask
            input_img = sample['mask'], \
                        torch.tensor(noise_fn(sample['mask'].clone().cpu().detach().numpy()))
            # input_img = input_img, torch.tensor(cmf.draw_lines(input_img.cpu().clone().detach().numpy()))
            target = input_img[0].clone().to(self.params['device']), \
                     input_img[1].clone().to(self.params['device'])

            fx_rang = list(range(3))  # [0, 1, 2] are the possible pixel values
            input_img = cmf.one_hot_encoding(input_img[0], fx_rang).to(self.params['device']), \
                        cmf.one_hot_encoding(input_img[1], fx_rang).to(self.params['device'])
        return input_img, target, filepath

    def get_loss(self, phase, model_out, target, batch_idx, n_imgs):
        """ Calculate the losses depending on the model phase.

        :return:
        """
        loss = 0
        if self.params['model_type'] in ['unet']:
            if self.params['ce_lambda'] > 0:  # Binary cross entropy between model output and target
                # target = torch.tensor(target, dtype=torch.long, device=self.params['device'])

                BCE = nn.BCELoss()(model_out, target)
                self.bce_loss_array.append(float(BCE))
                loss = loss + self.params['ce_lambda'] * BCE

            if self.models['acnn'] is not None:  # MSE between the codes (if ACNN loaded)
                # When training the AE, I've only passed to it hard segmentations, so it needs the partial outputs
                # to be realistic. For that, I'll keep the highest probability in each channel and then re-encode to
                # one hot, for having a similiar input as the one used during training.
                # inp = cmf.hard_segm_from_tensor(model_out).unsqueeze(1)  # todo Single Class may be different
                # inp = cmf.one_hot_encoding(cmf.hard_segm_from_tensor(model_out))  # Multi Class (change gs_img also)
                acnn_model_out_code, _ = self.models['acnn'].encode(model_out.clone().detach())

                # targt = cmf.hard_segm_from_tensor(target).unsqueeze(1)  # Single Class
                # targt = cmf.one_hot_encoding(target).to(self.params['device'])
                acnn_gt_code, _ = self.models['acnn'].encode(target.clone().detach())  # GT encoding

                mse = self.params['acnn_lambda'] * nn.MSELoss()(acnn_model_out_code,
                                                                acnn_gt_code)  # MSE Loss of the codes
                # mse = self.params['acnn_lambda'] * cmf.dice_loss()(acnn_model_out_code, acnn_gt_code)  # MSE Loss of the codes
                loss += mse  # Sum to other losses
                self.mse_loss_array.append(float(mse))

            if self.params['dice_lambda'] != 0:  # Dice loss
                dice = cmf.dice_loss()(model_out.clone().detach(), target.clone().detach())
                self.dice_loss_array.append(float(dice))
                loss = loss + dice

        elif self.params['model_type'] in ['acnn']:  # Autoencoder (ACNN) training
            if 'msel_lambda' not in self.params:
                raise KeyError("MSE lambda parameter (f_msel_lambda) not set in the .ini file.")

            normal_img, normal_gt = model_out[0], target[0].type(torch.LongTensor).to(self.params['device'])
            noised_img, noised_gt = model_out[1], target[1].type(torch.LongTensor).to(self.params['device'])

            code_normal_pred, _ = self.models['main'].encode(normal_img.clone().detach())
            code_noised_pred, _ = self.models['main'].encode(noised_img.clone().detach())

            ce_1 = nn.CrossEntropyLoss()(normal_img, normal_gt)
            ce_2 = nn.CrossEntropyLoss()(noised_img, noised_gt)
            ce_avg = ce_1 + ce_2  # Make sure I can reconstruct both

            msel = nn.MSELoss()(code_normal_pred, code_noised_pred)  # Make sure the encodings are different
            loss = .5 * ce_avg - self.params['msel_lambda'] * msel + .5 * cmf.brabandere_loss(
                [code_normal_pred, code_noised_pred])

        self.epoch_loss_array.append(float(loss))  # Display loss
        perc = 100. * (batch_idx + 1) / n_imgs
        print('    Batch {}/{} ({:.0f}%)\tLoss: {:.6f}'.format(batch_idx + 1, n_imgs, perc, loss))

        if self.params['save_dice_plots'] is True:
            self.dice_coef_array.append(cmf.dice_coeff(model_out, target))

        return loss

    def loss_avgs(self, phase):
        """ Save the loss average, and print the losses.

        :param phase:
        :return:
        """
        if phase in ['train', 'validation', 'val']:  # Only on training
            avg_loss = np.mean(self.epoch_loss_array)  # Average of all batches
            avgLossMsg = '\t Avg loss: {:.4f}'.format(avg_loss)

            avg_dice_coef = []
            if self.params['save_dice_plots']:
                avg_dice_coef = np.mean(self.dice_coef_array)

            if phase == 'train':
                self.train_loss.append(avg_loss)
                if self.params['save_dice_plots']:
                    self.dice_train.append(avg_dice_coef)
            elif phase == 'val':
                self.validation_loss.append(avg_loss)
                if self.params['save_dice_plots']:
                    self.dice_validation.append(avg_dice_coef)
            elif phase == 'test':
                self.test_loss.append(avg_loss)

            if self.params['model_type'] in ['unet', 'unetrec', 'flaprec', 'aerec', 'flaprecAE']:
                if self.params['ce_lambda'] > 0:
                    avg_bce = np.mean(self.bce_loss_array)  # Mean BCE for the epoch
                    avgLossMsg += ', Avg BCE.: {:.4f} (x{})'.format(avg_bce, self.params['ce_lambda'])
                if self.models['acnn'] is not None:  # Mean MSE for the epoch
                    avg_mse = np.mean(self.mse_loss_array)
                    avgLossMsg += ', Avg MSE.: {:.4f} (x{})'.format(avg_mse, self.params['acnn_lambda'])
                if self.params['dice_lambda'] != 0:  # Mean Dice for the epoch
                    avg_dice = np.mean(self.dice_loss_array)
                    avgLossMsg += ', Avg Dice.: {:.4f} (x{})'.format(avg_dice, self.params['dice_lambda'])
            print(avgLossMsg)


def models():
    ModelLoader('cfg/mc_acnn.ini')  # Autoencoder
    #  ModelLoader('cfg/mc_unet.cfg')  # U-Net w/o anatomical constraints
    # ModelLoader('cfg/mc_unet_acnn.cfg')  # U-Net with anatomical constraints

    # ModelLoader('cfg/l_acnn.cfg')  # Autoencoder
    #  ModelLoader('cfg/l_unet.cfg')  # U-Net w/o anatomical constraints
    #  ModelLoader('cfg/l_unet_acnn.cfg')  # U-Net with anatomical constraints


def tt_cmd():
    parser = argparse.ArgumentParser(description=textwrap.dedent('''\
         Train or test a model 
         --------------------------------
             Train or test a model for one of the following cases
                - mc_acnn.ini: Multi-class mask denoising autoencoder.
                - mc_unet.cfg: Multi-class U-Net.
                - mc_unet_acnn.cfg: Multi-class U-Net with ACNN regularization.
                
                - l_acnn.cfg: Single-class denoising autoencoder.
                - l_unet.cfg: Single-class U-Net.
                - l_unet_acnn.cfg: Single-class U-Net with ACNN regularization. 

               FIRST CONFIGURE THE CORRESPONDING FILE, and then run this command with the corresponding argument.
               The configuration can be performed in a text editor, and sample configurations can be found in the repo.
               You can't set more than one of these arguments at the same time.
         '''), formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-c', '--config', action='store_true', help="configuration file path")

    args = parser.parse_args()

    if np.count_nonzero(list(vars(args).values())) > 1:
        print("You have chosen more than one argument. Select only one of them.")
        return
    elif np.count_nonzero(list(vars(args).values())) == 0:
        print("You have to set an argument to continue. See help for more info.")
        return

    if args.config:
        modelLoader(args.config)

if __name__ == '__main__':
    tt_cmd()

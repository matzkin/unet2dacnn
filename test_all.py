"""  made for predicting in successive saved models """

import os
from dataset_classes import PNGDataset
from trainTestModel import ModelLoader
import torch


def predict_and_save(actual_name, model_folder, test_csv, epoch, model_type):
    loader = ModelLoader()
    loader.model_type = model_type
    loader.name = actual_name
    loader.models_path = model_folder
    loader.test_files_csv = test_csv
    loader.device = 'cuda'
    loader.img_identifier = 'i'
    loader.seg_identifier = 's'
    loader.cepoch = epoch
    loader.predict()


def test_model(model_name, model_folder, test_csv, model_type):
    for i in range(0, 1001):
        ep_name = model_name + '_ep{}'.format(i)
        real_name = os.path.join(model_folder, ep_name+'.pt')
        if os.path.exists(real_name):
            print('\nModel: {}'.format(real_name))
            predict_and_save(ep_name, model_folder, test_csv, i, model_type)
        else:
            print('\nModel: {} not found.'.format(real_name))


if __name__ == '__main__':
    test_csv_l = 'img/Test/Lung/Images/files.csv'

    model_folder = 'models/ep'

    # Single label
    # model_name = 'unet_acnn_200401'
    # test_model(model_name, model_folder, test_csv_l)

    # model_name = 'unet_acnn_200401_2'
    # test_model(model_name, model_folder, test_csv_l)

    # Multiclass
    model_name = 'unet_acnn_200330'
    test_csv_mc = 'img/Test/Lung/Images/files_mc.csv'
    test_model(model_name, model_folder, test_csv_l, 'unet')
#THIS FILE IS *VERY* OUTDATED

Python 3 update.
sudo apt-get install python3-tk


------------------

# deep-brain-extractor
A tool to perform automatic cranial cavity segmentation of CT images in NIfTI format.

Performed using Python 2.7 and PyTorch 0.4.

The implemented convolutional neural network is a variation of the [U-Net](https://arxiv.org/abs/1505.04597) model,
which was adapted to this particular problem, through modifications in the architecture and the learning process.

### Installation

The first step is to clone this repo, using git:

```sh
git clone https://gitlab.com/matzkin/deep-brain-extractor.git
```

After this, you must install all the required Python modules, in the required versions. For avoiding any conflict with the
moudles you may have installed, you can create a separate enviroment, or virtual enviroment trough: 

```sh
virtualenv --python=/usr/bin/python2.7 venv
```

A virtual enviroment called *venv* will be created in the current folder. After that, you can access this virtual 
enviroment using:

```sh
source venv/bin/activate
```

Now you can download and install all the dependencies using the requirements.txt file (included in the project) via:

```sh
pip install -r requirements.txt
```

This installs PyTorch 0.4.1 for CPU. If you prefer the GPU version, you'll have to remove the CPU version (through *pip
uninstall torch* and installing the *.whl* file provided in [their site](https://pytorch.org/get-started/previous-versions/)).

### Command line execution

Once you have all set up, you can predict the brain mask of a CT scan (*image.nii.gz* in this example) calling:

 ```sh
python DeepBrainExt.py -i image.nii.gz -o output_folder -n ouput_filename
```

Note that *output_folder* and *output_filename* arguments are optional. By default, the predicted mask will be placed
in the same folder of the input image, with the same filename but adding "_nnmask" before the extension (for this example it would be 
*image_nnmask.nii.gz*).

The help of the tool can be consulted via:

 ```sh
python DeepBrainExt.py --help
```
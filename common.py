# This file contains definitions of functions that can be shared across projects or are not inherent to the declared
# classes.

import configparser
import os
import random
import timeit
from shutil import copyfile

import cv2 as cv
import matplotlib.transforms
import numpy as np
import torch
import torch.nn as nn
from matplotlib import pyplot as plt
from torch.utils.data import DataLoader


def veri_folder(path):
    """ Verifies if the folder exists, if not, it will be created.

    :param path: folder to be checked/created.
    :return: path of the folder
    """
    if not os.path.exists(path):
        os.makedirs(path)
    return path


class dice_loss_legacy(nn.Module):
    def __init__(self):
        super(dice_loss_legacy, self).__init__()

    def forward(self, output, masks, eps=1):
        # b_size = masks.size(0)
        # num_classes = masks.size(1)
        probs = output
        mask = masks

        num = (probs.flatten() * mask.flatten()).sum()
        den1 = (probs.flatten() * probs.flatten()).sum()
        den2 = (mask.flatten() * mask.flatten()).sum()

        # num = (probs.view(b_size, -1) * mask.view(b_size, -1)).sum(1)
        # den1 = (probs.view(b_size, -1) * probs.view(b_size, -1)).sum(1)
        # den2 = (mask.view(b_size, -1) * mask.view(b_size, -1)).sum(1)

        return 1 - 2 * ((num + eps) / (den1 + den2 + eps))


class dice_loss(nn.Module):
    def __init__(self):
        super(dice_loss, self).__init__()

    def forward(self, output, ground_truth, eps=0.00001):
        bz = ground_truth.size(0)
        channels = ground_truth.size(1)
        loss = 0
        for channel in range(channels):
            num = (output[:, channel].view(bz, -1) * ground_truth[:, channel].view(bz, -1)).sum()
            den1 = (output[:, channel].view(bz, -1) * output[:, channel].view(bz, -1)).sum()
            den2 = (ground_truth[:, channel].view(bz, -1) * ground_truth[:, channel].view(bz, -1)).sum()

            loss += 2 / channels * ((num + eps) / (den1 + den2 + eps))

        return 1 - loss


def dice_coeff(pred, target):
    if type(pred) is tuple:
        dc = []
        for i in range(len(pred)):
            dc.append(dice_coeff(pred[i], target[i]))
        return np.mean(dc)

    target = one_hot_encoding(target).to('cuda') if pred.size() != target.size() else target
    b_size = target.size(0)
    eps = 0.0000001

    dice_arr = []
    for i in range(b_size):
        probs = pred[i][0]  # Since  pred[i][0] = 1 - pred[i][1]  I calculate it one time only.
        mask = target[i][0]

        num = (probs.flatten() * mask.flatten()).sum()
        den1 = (probs.flatten() * probs.flatten()).sum()
        den2 = (mask.flatten() * mask.flatten()).sum()

        dice = 2 * (num + eps) / (den1 + den2 + eps)
        dice_arr.append(dice.item())

    return np.mean(dice_arr)


# class BinaryDiceLoss(nn.Module):
#     """Dice loss of binary class
#     Args:
#         smooth: A float number to smooth loss, and avoid NaN error, default: 1
#         p: Denominator value: \sum{x^p} + \sum{y^p}, default: 2
#         predict: A tensor of shape [N, *]
#         target: A tensor of shape same with predict
#         reduction: Reduction method to apply, return mean over batch if 'mean',
#             return sum if 'sum', return a tensor of shape [N,] if 'none'
#     Returns:
#         Loss tensor according to arg reduction
#     Raise:
#         Exception if unexpected reduction
#     """
#
#     def __init__(self, smooth=1, p=2, reduction='mean'):
#         super(BinaryDiceLoss, self).__init__()
#         self.smooth = smooth
#         self.p = p
#         self.reduction = reduction
#
#     def forward(self, predict, target):
#         assert predict.shape[0] == target.shape[0], "predict & target batch size don't match"
#         predict = predict.contiguous().view(predict.shape[0], -1)
#         target = target.contiguous().view(target.shape[0], -1)
#
#         num = torch.sum(torch.mul(predict, target), dim=1) + self.smooth
#         den = torch.sum(predict.pow(self.p) + target.pow(self.p), dim=1) + self.smooth
#
#         loss = num / den
#
#         if self.reduction == 'mean':
#             return loss.mean()
#         elif self.reduction == 'sum':
#             return loss.sum()
#         elif self.reduction == 'none':
#             return loss
#         else:
#             raise Exception('Unexpected reduction {}'.format(self.reduction))
#
#
# class DiceLoss(nn.Module):
#     """Dice loss, need one hot encode input
#     Args:
#         weight: An array of shape [num_classes,]
#         ignore_index: class index to ignore
#         predict: A tensor of shape [N, C, *]
#         target: A tensor of same shape with predict
#         other args pass to BinaryDiceLoss
#     Return:
#         same as BinaryDiceLoss
#     """
#
#     def __init__(self, weight=None, ignore_index=None, **kwargs):
#         super(DiceLoss, self).__init__()
#         self.kwargs = kwargs
#         self.weight = weight
#         self.ignore_index = ignore_index
#
#     def forward(self, predict, target):
#         assert predict.shape == target.shape, 'predict & target shape do not match'
#         dice = BinaryDiceLoss(**self.kwargs)
#         total_loss = 0
#         predict = F.softmax(predict, dim=1)
#
#         for i in range(target.shape[1]):
#             if i != self.ignore_index:
#                 dice_loss = dice(predict[:, i], target[:, i])
#                 if self.weight is not None:
#                     assert self.weight.shape[0] == target.shape[1], \
#                         'Expect weight shape [{}], get[{}]'.format(target.shape[1], self.weight.shape[0])
#                     dice_loss *= self.weights[i]
#                 total_loss += dice_loss
#
#         return 1 - total_loss / target.shape[1]


def one_hot_encoding(pt_tensor, fixed_labels=None):
    """
    Given a hard segmentation PyTorch tensor, it returns the 1-hot encoding.

    :param pt_tensor: Batch of Tensor or tuple of batch of tensors to be encoded.
    :param fixed_labels: Labels to encode (optional).
    :return: Encoded batch (or tuple) of tensors.
    """
    if type(pt_tensor) is tuple:
        return tuple([one_hot_encoding(elem) for elem in pt_tensor])

    batch_size = pt_tensor.shape[0]
    hard_segm = pt_tensor.cpu().numpy()
    labels = np.unique(hard_segm) if fixed_labels is None else fixed_labels
    dims = hard_segm.shape

    uniq_labels = len(labels)

    one_hot = np.ndarray(shape=(batch_size, uniq_labels, dims[1], dims[2]), dtype=np.float32)

    # Transform the Hard Segmentation GT to one-hot encoding
    for j, labelValue in enumerate(labels):
        one_hot[:, j, :, :] = (hard_segm == labelValue).astype(np.int16)

    encoded = torch.from_numpy(one_hot)
    return encoded


def array_to_plot(losses, types, name='', plotTitle=None, x_label=None, y_label=None):
    # name: nombre del model
    # types: que perdidas hay en losses (training, validation, etc)

    lossPlotName = name + "_{}.png".format(y_label.lower())

    print("Saving plot... {}\n".format(lossPlotName))

    # Check if output folder exists
    veri_folder('img')

    bbox = matplotlib.transforms.Bbox([[-0.2, -0.36], [8, 5]])

    for i, arr in enumerate(losses):
        plt.plot(arr, label=types[i])

    if plotTitle is not None:
        plt.title(plotTitle)

    # Put titles to the axis
    if x_label is not None:
        plt.xlabel(x_label)

    if y_label is not None:
        plt.ylabel(y_label)

    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    plt.savefig(os.path.join('img', lossPlotName), dpi=200, bbox_inches=bbox)
    plt.close()


def save_model(model, path, cfg_file=None, epoch=-1, save_checkpoint=False):  # TODO This could be a class method
    """
    Save a PyTorch model

    :param model: Trained model
    :param path: Output path
    :param cfg_file: Configuration file, that will be saved in the first epoch
    :param epoch: Trained epochs of the model
    :param save_checkpoint: The method is called for saving a separate checkpoint (training continues).
    :return:
    """
    if save_checkpoint:
        checkpoint_path = path.replace('models', 'models/eps').replace('.pt', '_ep' + str(epoch) + '.pt')
        veri_folder(os.path.split(checkpoint_path)[0])

    veri_folder(os.path.split(path)[0])  # Check if output folder exists
    if cfg_file and epoch == 1:  # Save the model parameters alongside the model if is first epoch
        copyfile(cfg_file, path.replace('.pt', '_params.ini'))

    torch.save(model, path)  # Overwrite the output model
    if save_checkpoint:
        torch.save(model, checkpoint_path)  # Save the checkpoint in the eps folder
        print("Checkpoint saved ({})".format(save_checkpoint))

    print("Model saved ({})".format(path))


def hard_segm_from_tensor(prob_map):
    """
    Given a PyTorch tensor of probabilities, it returns a hard-segmentation image, which keeps as foreground the label
    of maximum probability.

    :param prob_map: probability map containing the per-class prediction.
    :return: If a 4D tensor is provided, it will take the first dimension as the batch index and will compute the hard-
             segmentation on the second one, otherwise it will calculate it on the first dimension.
    """
    if len(list(prob_map.shape)) == 4:
        prob_map = (torch.argmax(prob_map, dim=1)).type(torch.float)
    else:
        prob_map = (torch.argmax(prob_map, dim=0)).type(torch.float)
    return prob_map


def salt_and_pepper(img, noise_probability=1, noise_density=0.2, salt_ratio=0.1):
    """
    Given a numpy image, return a salt and pepper noised image.

    :param img: Image in which noise will be added. The first dimension will be taken as the batch element index.
    :param noise_probability: Probability of adding the noise (by default 1).
    :param noise_density: Amount of noise in the image (as a percentage).
    :param salt_ratio: Ratio of salt (vs pepper) added in the image.
    :return: Noised batch of images.
    """
    batch_size = img.shape[0]
    output = np.copy(img).astype(np.uint8)
    noise_density = np.random.uniform(0, noise_density)
    for i in range(batch_size):
        r = random.uniform(0, 1)  # Random number
        if noise_probability >= r:  # Inside the probability
            black_dots = (np.random.uniform(0, 1, output[i, :, :].shape) > noise_density * (1 - salt_ratio)).astype(
                np.uint8)
            white_dots = 1 - (np.random.uniform(0, 1, output[i, :, :].shape) > noise_density * salt_ratio).astype(
                np.uint8)
            output[i, :, :] = np.logical_and(output[i, :, :], black_dots)
            output[i, :, :] = np.logical_or(output[i, :, :], white_dots)
    return output


def draw_lines_single(x, swap_prob=0.6):
    noised = x  # Output starts from input
    width, height = x.shape

    n_10 = int(width * 10 / 1024)
    n_30 = int(width * 30 / 1024)
    n_60 = int(width * 60 / 1024)
    n_66 = int(width * 66 / 1024)
    n_80 = int(width * 80 / 1024)
    n_100 = int(width * 100 / 1024)
    n_150 = int(width * 150 / 1024)
    n_900 = int(width * 900 / 1024)
    n_950 = int(width * 950 / 1024)
    n_1024 = int(width)

    num_lines = np.random.randint(1, n_100)  # Pick a number N between 1-100
    for line in range(num_lines):  # Put N random white circles in output
        pos_x, pos_y = np.random.randint(n_100, n_950), np.random.randint(n_100, n_950)
        radius = np.random.randint(n_60)
        cv.circle(noised, (pos_x, pos_y), radius, color=(1, 1, 1), thickness=-1)  # Draw circle in noised

    k = np.random.randint(n_10, n_30)  # Square kernel size
    kernel = np.ones((k, k), np.uint8)  # Create square kernel
    noised = cv.morphologyEx(noised, cv.MORPH_CLOSE, kernel)  # Morphological closing with random sized square kernel

    num_lines = np.random.randint(1, n_80)
    for line in range(num_lines):  # Put N random black circles in output
        pos_x, pos_y = np.random.randint(n_150, n_900), np.random.randint(n_150, n_900)
        radius = np.random.randint(n_30)
        cv.circle(noised, (pos_x, pos_y), radius, color=(0, 0, 0), thickness=-1)  # Draw circle in noised

    if np.random.uniform(0, 1) > 0.3:  # Check if inside probability
        swap_map = np.random.choice(range(2), size=(n_1024, n_66),
                                    p=[1 - swap_prob, swap_prob])  # Slice of the image of rand. pixels
        h_idx, w_idx = np.where(swap_map == 1)  # Check which of those pixels is one
        w_idx = 15 * (w_idx + 1)
        bor = 15
        for b in range(bor):
            noised[h_idx, w_idx + b - bor] = noised[h_idx, w_idx - (1 + b)]
            noised[h_idx, w_idx - (1 + b)] = noised[h_idx, w_idx + b - bor]

    if np.random.uniform(0, 1) > 0.4:
        kernel = np.ones((1, 7), np.uint8)
        noised = cv.morphologyEx(noised, cv.MORPH_CLOSE, kernel)  # Closing with horizontal kernel

    return noised


def draw_lines(x, swap_prob=0.6):
    if len(x.shape) == 2:
        return draw_lines_single(x, swap_prob)

    one_channel = False
    if len(x.shape) == 3:
        one_channel = True
        x = np.expand_dims(x, 1)
    batch_size, channels, height, width = x.shape
    noised = x

    for i in range(batch_size):
        for j in range(channels):
            noised[i, j, :, :] = draw_lines_single(x[i, j, :, :], swap_prob)
    if one_channel:
        return noised[:, 0, :, :]
    else:
        return noised


def tic():
    return timeit.default_timer()


def toc_eps(ep_time, n_epoch, epochs, print_out=True):
    """
    Calculate training remaining time given a previous time, the current epoch and the total epochs.

    :param ep_time: Time per epoch (last)
    :param n_epoch: Number of epoch.
    :param epochs: Total of epochs.
    :param print_out: print the result
    """
    ep_time = timeit.default_timer() - ep_time
    time1 = int(ep_time * (epochs + 1 - n_epoch))
    time_h = time1 // 3600
    time_m = (time1 - time_h * 3600) // 60
    if print_out:
        print("({}%) Remaining time (HH:MM): {}:{}\n".format(int(100 * n_epoch / float(epochs)), time_h, time_m))
    return ep_time


def str_to_num(text, max_n=11):
    """
    Get a number from a string, based on the characters numeric code and a custom maximum set number.

    :param text: Input string.
    :param max_n: (One plus) the maximum number allowed.
    :return: Output number, in the range [0, max_n).
    """
    s = 0
    for c in text:
        s += ord(c)
    return s % max_n


def crop_str_from_n_ocurrence(string, chr='_', n=2):
    """
    Get a substring finding the nth occurrence of chr and slicing up to that position.
    Example: "image_to_process_training.png" with chr='_' and n=2 -> "image_to"

    :param string: Input string.
    :param chr: Separator character to find.
    :param n: Number of separators in the output substring.
    :return: Substring
    """
    idxs = [pos for pos, char in enumerate(string) if char == chr]
    if len(idxs) == 0:  # char not found
        return string
    idx = n if n <= len(idxs) else len(idxs)
    return string[:idxs[idx - 1]]


def set_cfg_params(cfg_file, default_dict=None):
    """
    From a .ini ConfigParser file, create a dictionary with its data in the corresponding data types. Currently int,
    float, bool and string (default) types are supported as prefixes in the .ini files.
    The first two chars of each variable name will identify the file type (i_, f_, b_ and s_ are supported).

    :param cfg_file: Path of the cfg file
    :param default_dict: Dictionary with the necessary and minimum parameters initialized with None values or similar
    (useful in the case the configuration file does not provide all the required params).
    :return: A dictionary with the parameters set in the .ini file (and the default ones if not changed and provided).
    """

    out_dict = default_dict if default_dict is not None else dict()  # Initialize the dictionary
    config = configparser.ConfigParser()
    config.read(cfg_file)

    for each_section in config.sections():
        for (key, value) in config.items(each_section):
            if key[:2] == "i_":  # int
                out_dict[key[2:]] = config[each_section].getint(key)
            elif key[:2] == "f_":  # float
                out_dict[key[2:]] = config[each_section].getfloat(key)
            elif key[:2] == "b_":  # bool
                out_dict[key[2:]] = config[each_section].getboolean(key)
            elif key[:2] == "s_":  # string
                out_dict[key[2:]] = value
            else:  # string by default
                out_dict[key] = value

    return out_dict


def get_dataloader(dataset_class, dataset_csv, batch_size=1, pin_memory=True, shuffle=True, required=False):
    """
    From a csv file, instance the dataset and get the DataLoader associated with it. If it's required and the csv is not
    found, it will raise an Exception.


    :param dataset_class: Class inherited of torch.utils.data.Dataset, that is used for creating the DataLader.
    :param dataset_csv: Path of the CSV.
    :param batch_size: Batch size used in the DataLoader.
    :param pin_memory: Dataloader's pin_memory flag, for loading the data into CUDA. This lets your DataLoader allocate
    the samples in page-locked memory, which speeds-up the transfer.
    :param shuffle: Shuffle the data in the DataLoader
    :param required: Raise an exception if the csv file is not found.
    :return:
    """
    if os.path.isfile(dataset_csv):
        dataset = dataset_class(dataset_csv, '')
        dataloader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=shuffle, pin_memory=pin_memory)
        return dataloader

    elif required:
        raise OSError("Dataset csv not found ({}).".format(dataset_csv))


def print_params_dict(dic):
    """
    Given a dicitonary, print its values in a table-like format.

    :param dic: Dictionary to print
    """
    print("{:<20} {:<30}".format('Parameter', 'Value'))
    for key in dic:
        v = dic[key]
        print("{:<15} {:<10}".format(key, str(v)))


def shape_2d(center, size, image_size, shape='circle'):
    """  Return a 2D numpy square or circumference given the center, size and image size. It creates a mask of the shape
        using the p-norm concept.

    :param center: array with the coordinates center of the shape.
    :param size: single number that represents the size of the shape in each dimension.
    :param image_size: size of the output array.
    :param shape: shape to return. Currently 'circle' and 'cube' are allowed.
    :return:
    """
    if shape == 'circle' or shape == 'sphere':
        ord = 2
    elif shape == 'square' or shape == 'box' or shape == 'cube':
        ord = np.inf
    else:
        print("Shape {} is not supported. Setting shape as sphere".format(shape))
        ord = 2
    distance = np.linalg.norm(np.subtract(np.indices(image_size).T, np.asarray(center)), axis=len(center), ord=ord)
    shape_np = 1 - np.ones(image_size).T * (distance <= size)
    return shape_np.T


def apply_random_mask(imgs_batch):
    """
    Use shape_2d for applying random masks in a batch of numpy images

    :param imgs_batch: batch of numpy images
    :return: batch with noised numpy images
    """
    for i, img in enumerate(imgs_batch):
        two_dims = len(img.shape) == 2
        im_shape = img.shape if two_dims else img.shape[1:]
        for _ in range(300):  # Search a feasible shape center
            center = np.random.randint(im_shape)
            pixel = img[tuple(center)] if two_dims else 1 - img[0][tuple(center)]
            if pixel:
                break
        size = np.random.randint(min(im_shape) / 64, min(im_shape) / 2)
        shape = ['circle', 'square'][np.random.randint(0, 2)]  # Pick a random shape
        shape_mask = shape_2d(center, size, im_shape, shape)
        if two_dims:
            imgs_batch[i] = img * shape_mask
        else:  # Encoded image
            for k, channel in enumerate(img):
                mask_channel = shape_mask if k != 0 else 1 - shape_mask  # Background channel has the opposite mask
                img[k] = channel * mask_channel
            imgs_batch[i] = img
    return imgs_batch


def brabandere_loss(pred, alpha=1, beta=1, gamma=1e-3, dv=.05, dd=2):
    """
    Implementation of the proposed loss in:
        Semantic Instance Segmentation with a Discriminative Loss Function (https://arxiv.org/abs/1708.02551)
        in which, the loss is a sum of three terms:
            1) A intra-cluster attraction force.
            2) A inter-cluster repealling force.
            3) A regularization term for the mean codes.
         This loss in unsupervised, so only batches of codes for each cluster are needed.

    :param pred: List of batches of embeddings from which the loss will be calculated.
    :param alpha: L_var (intra-cluster) loss weight.
    :param beta: L_dist (inter-cluster) loss weight.
    :param gamma: Regularization loss weight
    :return:
    """
    C = len(pred)  # Number of clusters
    o_loss = torch.zeros(3, dtype=torch.float, device='cuda:0')  # Lvar, Ldist, Lreg
    coeffs = torch.tensor([alpha, beta, gamma], dtype=torch.float, device='cuda:0')
    for c, cluster in enumerate(pred):  # Each cluster
        Nc = len(cluster)
        mean_cluster_a = torch.mean(cluster, dim=0)
        o_loss[0] = torch.tensor(0., device='cuda:0')  # Lvar
        for emb in cluster:  # Each embedding
            # Sum only if it's far from the cluster
            o_loss[0] += 1 / (C * Nc) * torch.pow(torch.max(torch.tensor(0., device='cuda:0'), torch.norm(mean_cluster_a - emb, 2) - dv),
                                                  2)
        # Ldist
        for j in range(c + 1, C):  # The other clusters
            mean_cluster_b = torch.mean(pred[j], dim=0)
            o_loss[1] += 1 / (C * C - C) * torch.pow(
                torch.max(torch.tensor(0., device='cuda:0'), 2 * dd - torch.norm(mean_cluster_a - mean_cluster_b, 2)), 2)

        o_loss[2] += 1 / C * torch.norm(mean_cluster_a, 2)  # Lreg

    return torch.dot(coeffs, o_loss)  # weighted loss

# class Brabandere_Loss(nn.Module):
#     def __init__(self):
#         super(Brabandere_Loss, self).__init__()
#
#     def forward(self, pred, alpha=1, beta=1, gamma=0.001, dv=1, dd=2):
#         """
#         Implementation of the proposed loss in:
#             Semantic Instance Segmentation with a Discriminative Loss Function (https://arxiv.org/abs/1708.02551)
#             in which, the loss is a sum of three terms:
#                 1) A intra-cluster attraction force.
#                 2) A inter-cluster repealling force.
#                 3) A regularization term for the mean codes.
#              This loss in unsupervised, so only batches of codes for each cluster are needed.
#
#         :param pred: List of batches of embeddings from which the loss will be calculated.
#         :param alpha: L_var (intra-cluster) loss weight.
#         :param beta: L_dist (inter-cluster) loss weight.
#         :param gamma: Regularization loss weight
#         :return:
#         """
#         C = len(pred)  # Number of clusters
#         o_loss = torch.zeros(3, dtype=torch.float)  # Lvar, Ldist, Lreg
#         coeffs = torch.tensor([alpha, beta, gamma])
#         for c, cluster in enumerate(pred):  # Each cluster
#             Nc = len(cluster)
#             mean_cluster_a = torch.mean(cluster, dim=0)
#             o_loss[0] = 0  # Lvar
#             for emb in cluster:  # Each embedding
#                 # Sum only if it's far from the cluster
#                 o_loss[0] += 1 / (C * Nc) * torch.pow(
#                     torch.max(torch.tensor(0.), torch.norm(mean_cluster_a - emb, 2) - dv),
#                     2)
#
#             o_loss[1] = 0  # Ldist
#             for j in range(c + 1, C):  # The other clusters
#                 mean_cluster_b = torch.mean(pred[j], dim=0)
#                 o_loss[1] += 1 / (C * C - C) * torch.pow(
#                     torch.max(torch.tensor(0.), 2 * dd - torch.norm(mean_cluster_a - mean_cluster_b, 2)), 2)
#
#             o_loss[2] += 1 / C * torch.norm(mean_cluster_a, 2)  # Lreg
#
#         return torch.dot(coeffs, o_loss)  # weighted loss

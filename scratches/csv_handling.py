# This file contains csv handling functions
import csv
import os


def csv_lungs(images_folder, labels_folder, csvname='files.csv', image_extension=".nii.gz"):
    filelist = [('image', 'mask')]  # CSV header

    for name in os.listdir(images_folder):
        f_ext = os.path.splitext(name)[1]
        if f_ext in image_extension:
            mask_name = name.replace(images_folder, labels_folder).replace(image_extension, '_labels' + image_extension)
            filepath = os.path.join(images_folder, name)
            filepath_m = os.path.join(labels_folder, mask_name)
            names = (filepath, filepath_m)
            filelist.append(names)
        else:
            continue  # Not an image file

    csv_path = os.path.join(images_folder, csvname)
    with open(csv_path, 'w') as fp:  # Save all files CSV
        writer = csv.writer(fp, delimiter=',')
        writer.writerows(filelist)

    print("Saved: ", csv_path)

    return csv_path


def csv_lungs_lucas(images_folder, csvname='files.csv', image_extension=".png"):
    filelist = [('image', 'mask')]  # CSV header

    for name in os.listdir(images_folder):
        if name.endswith('4_labels' + image_extension):
            filepath = os.path.join(images_folder, name)
            filepath_m = filepath
            names = (filepath, filepath_m)
            filelist.append(names)

    csv_path = os.path.join(images_folder, csvname)
    with open(csv_path, 'w') as fp:  # Save all files CSV
        writer = csv.writer(fp, delimiter=',')
        writer.writerows(filelist)

    print("Saved: ", csv_path)

    return csv_path

if __name__ == '__main__':
    csv_lungs_lucas()
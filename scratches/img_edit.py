# Simple script made for editing images

import cv2 as cv
import os
import numpy as np
import common as cmf


def prep_imgs(in_folder, out_folder=None, file_ext=None, intensity_map=None, image_res=None, interpolation=None):
    file_ext = '.png' if file_ext is None else file_ext
    out_folder = cmf.veri_folder(os.path.join(in_folder, 'out')) if out_folder is None else cmf.veri_folder(out_folder)
    interpolation = 'cv.INTER_NEAREST' if interpolation is None else interpolation
    intensity_map = dict() if intensity_map is None else intensity_map
    for file in os.listdir(in_folder):  # List all files in folder
        if file.endswith(file_ext):  # Check extension
            fpath = os.path.join(in_folder, file)
            out_file = os.path.join(out_folder, file)  # Create output file path
            img_o = cv.imread(fpath, 0)  # Load grayscale image
            for key_i in intensity_map.keys():
                img_o = np.where(img_o == key_i, intensity_map[key_i], img_o)  # Replace values as indicated
            if image_res:
                img_o = cv.resize(img_o, image_res, interpolation=interpolation)  # Resize image
            cv.imwrite(out_file, img_o)  # Save the resulting image in out_folder
            print("Saved image: {}.".format(out_file))


def convert_masks_lucas():
    in_folder = 'img/imgs_lucas'
    out_folder = 'img/imgs_lucas/processed'
    file_ext = '.png'
    intensity_map = {
        170: 2,
        85: 2,
        255: 1
    }
    image_res = (1024, 1024)
    interpolation = cv.INTER_NEAREST
    prep_imgs(in_folder, out_folder, file_ext, intensity_map, image_res, interpolation)


if __name__ == '__main__':
    convert_masks_lucas()

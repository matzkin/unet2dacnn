# This script loads a trained model and a group of datasets, and projects the data in a trained PCA (or T-SNE) for
# checking whether the plausible and unplausible classes are separable, at least in the first two components of these
# projections.

# For making this work, you should set the working directory to the previous folder

import warnings

import numpy as np
import seaborn as sns
import torch
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

import common as cmf
from dataset_classes import PNGDatasetC as PNGDataset

sns.set_style("darkgrid")

warnings.filterwarnings("ignore")


# def load_model_and_predict(model_path, files_csv):
#     dataloader = cmf.get_dataloader(PNGDataset, files_csv, batch_size=1, pin_memory=True, shuffle=True, required=True)
#     net = torch.load(model_path).eval()
#
#     code_size = 512
#     codes_normal = np.empty((0, code_size))
#     codes_noised = np.empty((0, code_size))
#     for batch_idx, sample in enumerate(dataloader):
#         img_normal = cmf.one_hot_encoding(sample['mask']).to('cuda')
#         img_noised = torch.tensor(cmf.draw_lines(img_normal.cpu().clone().detach().numpy())).to('cuda')
#
#         batch_code_normal = net.encode(img_normal)
#         batch_code_noised = net.encode(img_noised)
#         codes_normal = np.append(codes_normal, batch_code_normal[0].cpu().detach().numpy(), axis=0)
#         codes_noised = np.append(codes_noised, batch_code_noised[0].cpu().detach().numpy(), axis=0)
#
#     return codes_normal, codes_noised
#

# def plot_codes(codes, method='pca'):
#     codes_normal, codes_noised = codes
#     target = np.concatenate((len(codes_normal) * [0], len(codes_noised) * [1]))
#
#     all_codes = np.concatenate((codes_normal, codes_noised))
#     if method == 'pca':
#         pca = PCA(n_components=2)
#         pca.fit(all_codes)
#
#         X_pca = pca.transform(all_codes)
#
#         fig = plt.figure(figsize=(10, 8))
#         ax = fig.add_subplot(1, 1, 1)
#         scatter = ax.scatter(X_pca[:, 0],
#                              X_pca[:, 1],
#                              c=target,
#                              s=10,
#                              cmap=plt.get_cmap('flag'))
#         ax.set_xlabel("First principal component")
#         ax.set_ylabel("Second principal component")
#
#         plt.title('First two components of the PCA.')
#         plt.show()
#
#
# def check_codes(model_path, files_csv):
#     codes = load_model_and_predict(model_path, files_csv)
#     plot_codes(codes, method='pca')


def get_codes(model, dataloader, apply_noise=False, code_size=512):
    """
    From a PyTorch dataloader, it encodes the images using the provided model.

    :param code_size: Code size from which the AE reconstructs the image.
    :param model: AE trained model (which has the 'encode' method).
    :param dataloader: Dataloader of the corresponding images (only the masks will be used).
    :param apply_noise: Use draw_lines() for making the image unplausible.
    :return: numpy array containing the codes for all the data in the Dataloader.
    """
    codes = np.empty((0, code_size))
    for batch_idx, sample in enumerate(dataloader):
        if not apply_noise:
            img = cmf.one_hot_encoding(sample['mask'], fixed_labels=[0, 1, 2]).to('cuda')
        else:
            img = torch.tensor(cmf.draw_lines(cmf.one_hot_encoding(sample['mask']).cpu().detach().numpy())).to('cuda')

        batch_code_normal = model.encode(img)
        codes = np.append(codes, batch_code_normal[0].cpu().detach().numpy(), axis=0)
    return codes


def load_model_and_predict_dict(model_path, list_of_datasets_dict):
    net = torch.load(model_path).eval()  # Load trained model

    out_codes = []
    for ds in list_of_datasets_dict:  # For each dataset
        dataloader = cmf.get_dataloader(PNGDataset, ds['csv_path'], batch_size=1, pin_memory=True, shuffle=True,
                                        required=True)

        codes_dict = {'name': ds['name'], 'codes': get_codes(net, dataloader), 'codes_augm': None,
                      'plausible': ds['plausible'], 'custom_color': ds['custom_color'],
                      'use_for_fit': ds['use_for_fit']}

        if ds['augment']:
            codes_dict['codes_augm'] = get_codes(net, dataloader, apply_noise=True)

        out_codes.append(codes_dict)

    return out_codes


def check_codes_dict(model_path, files_csv, method='tsne'):
    codes = load_model_and_predict_dict(model_path, files_csv)
    plot_codes_dict(codes, method)


def plot_codes_dict(codes_dict_list, method='tsne'):
    """
    Given a codes dictionary list, it plots the two components that capture the more variance.

    :param codes_dict_list: list that contains the codes oganized in a dict with the format
            {'name': .., 'codes': .., 'codes_augm': ..}
    :param method: Method used for separating the data. Currently only PCA and TSNE are supported.
    :return:
    """
    fig = plt.figure()
    pl_col = '#34eb80'  # plausible color: green
    unpl_col = '#eb4634'  # unplausible color: red
    data_x_train = np.array([])
    data_y_train = np.array([])
    labels_train = np.array([])
    for cod_d in codes_dict_list:  # Create the training array
        # Set the label
        color = pl_col if cod_d['plausible'] else unpl_col  # Set default color (green/red)
        color = cod_d['custom_color'] if cod_d['custom_color'] is not None else color  # Set custom color if provided.

        if cod_d['use_for_fit']:  # Create the training data (a subset of the test)
            data_x_train = cod_d['codes'] if data_x_train.size == 0 else np.concatenate((data_x_train, cod_d['codes']))

            colors_l = np.array(len(cod_d['codes']) * [color])
            data_y_train = colors_l if data_y_train.size == 0 else np.concatenate((data_y_train, colors_l))

            names_l = np.array(len(cod_d['codes']) * [cod_d['name']])
            labels_train = names_l if labels_train.size == 0 else np.concatenate((labels_train, names_l))

            if cod_d['codes_augm'] is not None:  # If there's also the an augmented unplausible version
                data_x_train = np.concatenate((data_x_train, cod_d['codes_augm']))
                data_y_train = np.concatenate((data_y_train, len(cod_d['codes_augm']) * [unpl_col]))

    if method == 'pca':

        pca = PCA(n_components=2)
        pca.fit(data_x_train)

        ax = fig.add_subplot(1, 1, 1)

        for cod_d in codes_dict_list:  # Scatter all the points (incl. training)
            X_pca = pca.transform(cod_d['codes'])
            color = pl_col if cod_d['plausible'] else unpl_col  # Set default color (green/red)
            color = cod_d['custom_color'] if cod_d[
                                                 'custom_color'] is not None else color  # Set custom color if provided.
            ax.scatter(X_pca[:, 0], X_pca[:, 1],
                       c=color,
                       s=15,
                       cmap=plt.get_cmap('flag'),
                       label=cod_d['name'])

            if cod_d['codes_augm'] is not None:  # If there's also the an augmented unplausible version
                X_pca = pca.transform(cod_d['codes_augm'])
                color = unpl_col
                ax.scatter(X_pca[:, 0], X_pca[:, 1],
                           c=color,
                           s=15,
                           cmap=plt.get_cmap('flag'),
                           label=cod_d['name'].replace('plausible', 'unplausible-generated'))

        ax.legend()

        ax.set_xlabel("First principal component")
        ax.set_ylabel("Second principal component")

        ax.legend()

        plt.title('First two components of the PCA.')

    elif method == 'tsne':
        tsne = TSNE(n_components=2)

        X_tsne = tsne.fit_transform(data_x_train)
        colors = data_y_train

        ax = fig.add_subplot(1, 1, 1)
        scatter = ax.scatter(X_tsne[:, 0], X_tsne[:, 1], s=15, c=colors)

        ax.legend(*scatter.legend_elements())

        # ax.legend()

        ax.set_xlabel("First TSNE component")
        ax.set_ylabel("Second TSNE component")

        plt.title('First two components of the TSNE.')

    plt.show()


if __name__ == '__main__':
    # Model
    model_path = 'models/acnn_0619_ep1000.pt'
    # model_path = '/home/franco/Escritorio/modelos/acnn_0526.pt'  # Best one
    # model_path = '/home/franco/Escritorio/modelos/acnn_0426_diceavg_mse.pt'

    # Used datasets
    # example = {'name': 'Name of the dataset',
    #            'csv_path': 'Path of the csv with the files in the format 'image_path', 'mask_path'.',
    #            'augment': True if the images are used for creating unplausible masks with draw_lines,
    #            'plausible': True if the dataset consists in plausible images.}
    testJSRT = {'name': 'dataset test set (plausible)',
           'csv_path': 'img/Test/Lung/Images/files_mc.csv',
           'augment': True,
           'plausible': True,
           'custom_color': None,
           'use_for_fit': True}
    lucasP1 = {'name': 'lucas dataset (0, plausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/imgs_lucas/processed/files_0.csv',
           'augment': False,
           'plausible': True,
           'custom_color': '#03fc8c',
           'use_for_fit': True}
    lucasP2 = {'name': 'lucas dataset (1, plausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/imgs_lucas/processed/files_1.csv',
           'augment': False,
           'plausible': True,
           'custom_color': '#03fcdf',
           'use_for_fit': True}
    lucasU1 = {'name': 'lucas dataset (2, unplausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/imgs_lucas/processed/files_2.csv',
           'augment': False,
           'plausible': False,
           'custom_color': '#3d03fc',
           'use_for_fit': True}
    lucasU2 = {'name': 'lucas dataset (3, unplausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/imgs_lucas/processed/files_3.csv',
           'augment': False,
           'plausible': False,
           'custom_color': '#9403fc',
           'use_for_fit': True}
    lucasU3 = {'name': 'lucas dataset (4, unplausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/imgs_lucas/processed/files_4.csv',
           'augment': False,
           'plausible': False,
           'custom_color': '#fc03f8',
           'use_for_fit': True}
    dsRF = {'name': 'Random Forest (unplausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/unplausible/RF/files.csv',
           'augment': False,
           'plausible': False,
           'custom_color': '#61322b',
           'use_for_fit': True}
    dsUNet5 = {'name': 'UNet 5 epochs (unplausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/unplausible/UNEt_5_ep/files.csv',
           'augment': False,
           'plausible': False,
           'custom_color': '#665351',
           'use_for_fit': True}
    dsUNet10 = {'name': 'UNet 10 epochs (unplausible)',
           'csv_path': '/home/franco/Code/unet2Dacnn/img/unplausible/Unet_10_ep/files.csv',
           'augment': False,
           'plausible': False,
           'custom_color': '#701309',
           'use_for_fit': True}

    # ds = [ds1]
    ds = [testJSRT,
          lucasP1, lucasP2, lucasU1, lucasU2, lucasU3,
          dsRF, dsUNet5, dsUNet10]
    # ds = [ds2, ds3, ds4, ds5, ds6]
    check_codes_dict(model_path, ds, method='pca')
    # check_codes(model_path, files_csv, title)

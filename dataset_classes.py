# In this file I'll put the PyTorch Dataset definitions, needed for proper loading of images.
# It's recommended to declare data augmentation here, using inheritance and keeping a simple instancing interface.

import os

import cv2 as cv
import pandas as pd
import torch
from torch.utils.data import Dataset


class PNGDataset(Dataset):
    def __init__(self, csv_file=None, root_dir=None, images_are_rgb=False, resize=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
        """
        if csv_file is not None:
            self.files_frame = pd.read_csv(csv_file, )
        else:
            self.files_frame = None
        self.imread_color_img = 1 if images_are_rgb else 0
        self.root_dir = root_dir
        self.resize = resize

    def __len__(self):
        return len(self.files_frame)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.files_frame.iloc[idx, 0])
        seg_name = os.path.join(self.root_dir, self.files_frame.iloc[idx, 1])

        img = torch.tensor(cv.imread(img_name, self.imread_color_img), dtype=torch.float)
        # seg = torch.tensor(cv.imread(seg_name, 0), dtype=torch.float) if os.path.isfile(seg_name) else 0 * img.clone()
        seg = cv.imread(seg_name, 0)
        if self.resize:
            seg = cv.resize(seg, (self.resize, self.resize), interpolation=cv.INTER_NEAREST)
        seg = torch.tensor(seg, dtype=torch.float)

        # Transforms could be added in this step.

        img = img.unsqueeze(0) if self.imread_color_img == 0 else img.permute(2, 0, 1)  # Add channel dim or swap it

        return {'image': img, 'mask': seg, 'filepath': img_name}


class PNGDatasetC(PNGDataset):
    def __init__(self, csv_file=None, root_dir=None):
        super().__init__(csv_file, root_dir, images_are_rgb=True)


class PNGDatasetBW(PNGDataset):
    def __init__(self, csv_file=None, root_dir=None):
        super().__init__(csv_file, root_dir, images_are_rgb=False)

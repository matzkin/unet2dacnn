import torch.nn as nn
import torch.nn.functional as F
import torch

from dataset_classes import PNGDatasetC
import common as cmf
import numpy as np

def down_block_ae(in_c, out_c):
    return nn.Sequential(nn.Conv2d(in_c, out_c, kernel_size=3, padding=1, stride=2),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True),
                         nn.Conv2d(out_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True))


def up_block_ae(in_c, out_c):
    return nn.Sequential(nn.ConvTranspose2d(in_c, in_c, kernel_size=2, stride=2),
                         nn.Conv2d(in_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True),
                         nn.Conv2d(out_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True))


def up_block_ae2(in_c, out_c=2):
    return nn.Sequential(nn.ConvTranspose2d(in_c, in_c, kernel_size=2, stride=2),
                         nn.Conv2d(in_c, in_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(in_c),
                         nn.ReLU(True),
                         nn.Conv2d(in_c, out_c, kernel_size=1, padding=0, stride=1),
                         nn.Sigmoid())


class acnn_AE(nn.Module):
    def __init__(self, kernel_sz=3):
        super(acnn_AE, self).__init__()

        self.db1 = down_block_ae(1, 16)
        self.db2 = down_block_ae(16, 32)
        self.db3 = down_block_ae(32, 32)
        self.db4 = down_block_ae(32, 32)
        self.db5 = nn.Conv2d(32, 32, kernel_size=3, stride=2, padding=1)

        self.fc1 = nn.Linear(32768, 1024)
        self.fc2 = nn.Linear(1024, 32768)

        self.ub1 = up_block_ae(32, 16)
        self.ub2 = up_block_ae(16, 16)
        self.ub3 = up_block_ae(16, 16)
        self.ub4 = up_block_ae(16, 16)
        self.ub5 = up_block_ae2(16, 2)

    def encode(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)

        x = self.db1(x)
        x = self.db2(x)
        x = self.db3(x)
        x = self.db4(x)
        x = self.db5(x)
        x = F.relu(x)

        shape_fc = list(x.shape)  # Save for later
        x = x.view(o_shape[0], -1)

        code = self.fc1(x)

        return code, shape_fc

    def decode(self, code, shape_fc):
        x = self.fc2(code)
        x = F.relu(x)
        x = x.view(shape_fc)

        x = self.ub1(x)
        x = self.ub2(x)
        x = self.ub3(x)
        x = self.ub4(x)
        x = self.ub5(x)

        return x

    def forward(self, x):
        code, shape_fc = self.encode(x)
        result = self.decode(code, shape_fc)

        return result


class acnn_AE_3(nn.Module):
    def __init__(self, kernel_sz=3):
        super(acnn_AE_3, self).__init__()

        self.db1 = down_block_ae(3, 16)
        self.db2 = down_block_ae(16, 32)
        self.db3 = down_block_ae(32, 32)
        self.db4 = down_block_ae(32, 32)
        self.db5 = nn.Conv2d(32, 32, kernel_size=3, stride=2, padding=1)

        fc_in = 32768  # Size for 1024x1024 images
        code_size = 512
        # fc_in = 128  # Size for 64x64 images
        # code_size = 64
        self.fc1 = nn.Linear(fc_in, code_size)
        self.bn1 = nn.BatchNorm1d(code_size)
        self.fc2 = nn.Linear(code_size, fc_in)

        self.ub1 = up_block_ae(32, 16)
        self.ub2 = up_block_ae(16, 16)
        self.ub3 = up_block_ae(16, 16)
        self.ub4 = up_block_ae(16, 16)
        self.ub5 = up_block_ae2(16, 3)

    def encode(self, x):
        o_shape = x.shape  # (batch_size, channels, x, y, z)

        x = self.db1(x)
        x = self.db2(x)
        x = self.db3(x)
        x = self.db4(x)
        x = self.db5(x)
        x = F.relu(x)

        shape_fc = list(x.shape)  # Save for later
        x = x.view(o_shape[0], -1)

        code = F.relu(self.fc1(x))

        return code, shape_fc

    def decode(self, code, shape_fc):
        x = self.fc2(code)
        x = F.relu(x)
        x = x.view(shape_fc)

        x = self.ub1(x)
        x = self.ub2(x)
        x = self.ub3(x)
        x = self.ub4(x)
        x = self.ub5(x)

        return x

    def forward(self, x):
        code, shape_fc = self.encode(x)
        result = self.decode(code, shape_fc)

        return F.softmax(result, dim=1)


def down_block_unet(in_c, out_c, do_p=0.):
    return nn.Sequential(nn.Conv2d(in_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True),
                         nn.Conv2d(out_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True),
                         nn.Dropout2d(do_p))


def up_block_unet(in_c, out_c, do_p=0.):
    return nn.Sequential(nn.ConvTranspose2d(in_c, in_c, kernel_size=2, stride=2),
                         nn.Conv2d(in_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True),
                         nn.Conv2d(out_c, out_c, kernel_size=3, padding=1, stride=1),
                         nn.BatchNorm2d(out_c),
                         nn.ReLU(True),
                         nn.Dropout2d(do_p))


def lc_unet(in_c, out_c=2):
    return nn.Sequential(nn.Conv2d(in_c, out_c, kernel_size=1, padding=0, stride=1), 
                         nn.Sigmoid())


class unet(nn.Module):
    def __init__(self):
        super(unet, self).__init__()

        self.mp = nn.MaxPool2d(kernel_size=2, padding=0, stride=2, dilation=1)

        self.db1 = down_block_unet(1, 8)
        self.db2 = down_block_unet(8, 16)
        self.db3 = down_block_unet(16, 32)
        self.db4 = down_block_unet(32, 64)

        self.cblock = nn.Sequential(nn.Conv2d(64, 128, kernel_size=3, padding=1),
                                           nn.ReLU(True),
                                           nn.Conv2d(128, 128, kernel_size=3, padding=1),
                                           nn.ReLU(True))

        self.ub1 = up_block_unet(128, 64)
        self.ub2 = up_block_unet(128, 32)
        self.ub3 = up_block_unet(64, 16)
        self.ub4 = up_block_unet(32, 8)
        self.lc = lc_unet(16)

    def forward(self, x):
        down1 = self.db1(x)
        down1_mp = self.mp(down1)
        down2 = self.db2(down1_mp)
        down2_mp = self.mp(down2)
        down3 = self.db3(down2_mp)
        down3_mp = self.mp(down3)
        down4 = self.db4(down3_mp)
        down4_mp = self.mp(down4)

        center = self.cblock(down4_mp)

        up1 = self.ub1(center)
        up1 = torch.cat((up1, down4), 1)
        up2 = self.ub2(up1)
        up2 = torch.cat((up2, down3), 1)
        up3 = self.ub3(up2)
        up3 = torch.cat((up3, down2), 1)
        up4 = self.ub4(up3)
        up4 = torch.cat((up4, down1), 1)
        lc = self.lc(up4)

        return F.softmax(lc, dim=1)


class unet_3(nn.Module):
    def __init__(self):
        super(unet_3, self).__init__()

        self.mp = nn.MaxPool2d(kernel_size=2, padding=0, stride=2, dilation=1)

        i_features = 8
        do_p = 0.25

        self.db1 = down_block_unet(3, i_features, do_p=do_p)
        self.db2 = down_block_unet(i_features, 2*i_features, do_p=do_p)
        self.db3 = down_block_unet(2*i_features, 4*i_features, do_p=do_p)
        self.db4 = down_block_unet(4*i_features, 8*i_features, do_p=do_p)

        self.cblock = nn.Sequential(nn.Conv2d(8*i_features, 16*i_features, kernel_size=3, padding=1),
                                    nn.BatchNorm2d(16*i_features),
                                    nn.ReLU(True),
                                    nn.Conv2d(16*i_features, 16*i_features, kernel_size=3, padding=1),
                                    nn.BatchNorm2d(16*i_features),
                                    nn.ReLU(True))
        self.do2 = nn.Sequential(nn.Dropout2d(do_p))

        self.ub1 = up_block_unet(16*i_features, 8*i_features, do_p=do_p)
        self.ub2 = up_block_unet(16*i_features, 4*i_features, do_p=do_p)
        self.ub3 = up_block_unet(8*i_features, 2*i_features, do_p=do_p)
        self.ub4 = up_block_unet(4*i_features, i_features, do_p=do_p)
        self.lc = lc_unet(2*i_features, 3)

    def forward(self, x):
        down1 = self.db1(x)
        down1_mp = self.mp(down1)
        down2 = self.db2(down1_mp)
        down2_mp = self.mp(down2)
        down3 = self.db3(down2_mp)
        down3_mp = self.mp(down3)
        down4 = self.db4(down3_mp)
        down4_mp = self.mp(down4)

        center = self.cblock(down4_mp)

        up1 = self.do2(self.ub1(center))
        up1 = torch.cat((up1, down4), 1)
        up2 = self.ub2(up1)
        up2 = torch.cat((up2, down3), 1)
        up3 = self.ub3(up2)
        up3 = torch.cat((up3, down2), 1)
        up4 = self.ub4(up3)
        up4 = torch.cat((up4, down1), 1)
        lc = self.lc(up4)

        return lc


class ACNNComplete(acnn_AE_3):
    def __init__(self):
        super(ACNNComplete, self).__init__()

    def get_dataset(self):
        return PNGDatasetC

    def get_input_and_target(self, sample, phase):
        noise_fn = [cmf.apply_random_mask, cmf.draw_lines][np.random.randint(0, 2)]
        # noise_fn = cmf.apply_random_mask
        input_img = sample['mask'], \
                    torch.tensor(noise_fn(sample['mask'].clone().cpu().detach().numpy()))
        # input_img = input_img, torch.tensor(cmf.draw_lines(input_img.cpu().clone().detach().numpy()))
        target = input_img[0].clone().to(self.params['device']), \
                 input_img[1].clone().to(self.params['device'])

        fx_rang = list(range(3))  # [0, 1, 2] are the possible pixel values
        input_img = cmf.one_hot_encoding(input_img[0], fx_rang).to(self.params['device']), \
                    cmf.one_hot_encoding(input_img[1], fx_rang).to(self.params['device'])

        return input_img, target, filepath
    def get_loss(self):
        pass

    def write_predictions(self):
        pass

    def read_cfg(self):
        pass

    def plot_losses(self):
        pass